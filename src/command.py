import os
import subprocess
import datetime
from shell import shell_colors

class Command():
    """Classe pour l'exécution de commandes dans un shell UNIX
    """

    def __init__(self, cmd:str, save_output:bool=False) -> None:
        """Constructeur de la classe Command

        :param cmd: commande à lancer
        :type cmd: str
        :param save_output: détermine l'enregistrement de la réponse à la commande dans un fichier du dossier aero360_logs, defaults to False
        :type save_output: bool, optional
        """
        self.save_output = save_output
        self.cmd = cmd
        self.exec = False
        self.logs = ''

    def run(self) -> bool:
        """Lance l'éxecution de la commande

        :return: booléen qui traduit le succès de l'exécution de la commande
        :rtype: bool
        """

        print(self.color_log(f"AERO360 : la commande \"{self.cmd}\" est en train d'être exécutée...", event='warning'))

        self.response = subprocess.run([self.cmd], capture_output=True, text=True, check=False, shell=True) #<shell=True> : la commande est exécutée comme une chaîne de caractères
        
        if self.response.returncode:
            log = f"AERO360 : la commande n'a pas pu être exécutée correctement."
            self.logs += log
            print(self.color_log(log, event='fail'))
            self.sucess = False
        
        else:
            log = f"AERO360 : la commande a été exécutée avec succès."
            self.logs += log
            print(self.color_log(log, event='okgreen'))
            self.sucess = True

        if self.save_output:
            self.save()
            
        return self.exec
    
    def color_log(self, log:str, event:'str'='warning') -> str:
        """Méthode pour colorer un log en fonction du type d'évenement déclenché par la commande

        :param log: log à colorer
        :type log: str
        :param event: type évenement déclenché par la commande, defaults to 'warning'
        :type event: str, optional
        :return: log coloré
        :rtype: str
        """
        if event not in shell_colors.keys():
            return log
        else:
            colored_log = shell_colors[event] + log.split(':')[0] + ':' + shell_colors['endc'] + log.split(':')[1]
            return colored_log

    def save(self):
        """Enregistre les logs de l'éxecution de la commande dans un fichier commun à toutes les commandes lancées dans l'heure courante
        """

        self.response_timestamp = datetime.datetime.now().strftime('%Y%m%d%H')
        dir_ = os.getcwd() + '/aero360_logs/'

        if not os.path.exists(dir_):
            subprocess.run(['mkdir aero360_logs'], shell=True)

        file_name = f'log_{self.response_timestamp}.txt'

        if not os.path.exists(f'{dir_}{file_name}'):
            with open(f'{dir_}{file_name}', encoding='utf-8', mode='a') as fd:
                fd.write(f'###--------LOGS DU TIMESTAMP : {self.response_timestamp}--------###\n')

        with open(f'{dir_}{file_name}', encoding='utf-8', mode='a') as fd:
            fd.write(self.logs)
            fd.write(f"\nCOMMANDE : {self.cmd}\n")
            
            if self.response.returncode:
                fd.write(f"\nREPONSE : {self.response.stderr}\n")
            else:
                fd.write(f"\nREPONSE : {self.response.stdout}\n")

            fd.write('###--------------------------------------------------------------###\n')