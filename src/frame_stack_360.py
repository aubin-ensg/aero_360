from command import Command
from log_aero360 import LogAero360
from cam_360 import Cam360
from pathlib import Path
from shell import shell_colors
import numpy as np
from PIL import Image
import matplotlib.image
import glob
import equi2

class FrameStack360():
    """Classe qui contient un paquet de frames extraites d'une PVA360 et générées par les méthode smart_frames() et dumb_frames()
    """

    def __init__(self,path:str) -> None:
        """Constructeur de la classe FrameStack360

        :param path: chemin vers le dossier contenant les frames
        :type path: str
        """
        self.path = path
        self.frames = glob.glob(f'{self.path}/*.png')

    def deproj_frames(self, overwrite:bool=True) -> None:
        """Déprojete toutes les images du paquet de l'EAC vers des images binoculaires fish-eye

        :param overwrite: détermine la supression de dossiers déjà créés, defaults to True
        :type overwrite: bool, optional
        """

        if not overwrite:
            print(LogAero360('Images déjà déprojetées.', event='okblue'))
            return

        print(LogAero360(f'Découpage des images projetées (lentille gauche/ lentille droite)...', event='okblue'))
        
        for img in self.frames:
            sucess = equi2.equi_2_split_equi(img)
            if sucess:
                print(LogAero360(f"L'image {img.split('/')[-1]} a été découpée avec succès.", event='okgreen'))
            else:
                print(LogAero360(f"Le découpage de l'image {img.split('/')[-1]} a échoué.", event='fail'))

        mkdir_cmd = Command(f'cd {self.path} && rm -rf left right && mkdir left right')
        mkdir_cmd.run()

        mv_cmd = Command(f'cd {self.path} && mv *_left.png left && mv *_right.png right')
        mv_cmd.run()

        print(LogAero360(f'Découpage des images terminé.', event='okblue'))

        print(LogAero360(f'Déprojection des images découpées...', event='okblue'))

        equi_images_left = glob.glob(f'{self.path}/left/*png')
        for img in equi_images_left:
            sucess = equi2.equi_2_fish_eye(img)
            if sucess:
                print(LogAero360(f"L'image {img.split('/')[-1]} a été déprojetée avec succès.", event='okgreen'))
            else:
                print(LogAero360(f"La déprojection de l'image {img.split('/')[-1]} a échouée.", event='fail'))

        equi_images_right = glob.glob(f'{self.path}/right/*png')
        for img in equi_images_right:
            sucess = equi2.equi_2_fish_eye(img)
            if sucess:
                print(LogAero360(f"L'image {img.split('/')[-1]} a été déprojetée avec succès.", event='okgreen'))
            else:
                print(LogAero360(f"La déprojection de l'image {img.split('/')[-1]} a échouée.", event="fail"))

        print(LogAero360(f'Déprojection des images terminée.', event='okblue'))

        clean_deproj_cmd = Command(f'cd {self.path} rm ./left/*_left.png ./right/*_right.png')
        clean_deproj_cmd.run()

    def concat_left_right(self) -> None:
        """Concatène les images des deux lentilles une fois déprojetées
        """

        print(LogAero360('La concaténation des images est lancée...', 'bold'))
        left_fish_eye_images = glob.glob(f'{self.path}/left/*_fish_eye.png')

        for left_img in left_fish_eye_images:
            name = left_img.split('/')[-1]
            right_img = f"{self.path}/right/{name[:name.find('_left')]}_right_fish_eye.png"
            concat = equi2.concat(left_img, right_img, f"{self.path}/{name[:name.find('_left')]}.png")

            if concat.size != 0 :
                print(LogAero360(f"Les images {left_img.split('/')[-1]} et {right_img.split('/')[-1]} ont été concaténées.", event='okgreen'))
            else:
                print(LogAero360(f"La des images {left_img.split('/')[-1]} et {right_img.split('/')[-1]} concaténation a échouée", event='fail'))

        clean_concat_cmd = Command(f'cd {self.path} && rm -rf left right')
        clean_concat_cmd.run()

        self.stereo_frames = glob.glob(f'{self.path}/*.png')

        print(LogAero360('La concaténation des images est terminée.', 'bold'))


    def set_exif(self, cam_360:Cam360) -> None:
        """Enrichi les métadonnées des frames du paquet à partir de celles de la caméra utilisée 

        :param cam_360: caméra utilisée
        :type cam_360: Cam360
        """
        set_exif_cmd = Command(f'cd {self.path} && mm3d SetExif ".*png" F={cam_360.f} F35={cam_360.f_35}')
        set_exif_cmd.run()

    def tapioca(self, mode:str='Line', pattern:str='.*png', img_size:int=-1, adjacent_imgs=3) -> None:
        """Lance la commande Tapioca de MicMac avec les images du paquet

        :param mode: mode de prise de vue, defaults to 'Line'
        :type mode: str, optional
        :param pattern: pattern des images à traiter, defaults to '.*png'
        :type pattern: str, optional
        :param img_size: taille des images, defaults to -1
        :type img_size: int, optional
        :param adjacent_imgs: nombre d'images adjacentes, defaults to 3
        :type adjacent_imgs: int, optional
        """
        tapioca_cmd = Command(f'cd {self.path} && mm3d Tapioca {mode} {pattern} {img_size} {adjacent_imgs}')
        tapioca_cmd.run()

    def tapas(self) -> None:
        """Lance la commande Tapas de MicMac
        """
        pass

    def saisie_masq(self) -> None:
        """Lance la saisie d'un masque gauche/droite pour le traitement indépendant des deux lentilles ; crée les répertoires 'left' et 'right' en conséquence
        """

        saisie_masq_left_cmd = Command(f'cd {self.path} && mm3d SaisieMasqQT {self.frames[0]} Name="masq_left.tif"')
        saisie_masq_left_cmd.run()

        mkdir_cmd = Command(f'cd {self.path} && mkdir left && cp *.png left && cp -r Homol Pastis left && mv masq_left.* left', save_output=True)
        mkdir_cmd.run()

        saisie_masq_right_cmd = Command(f'cd {self.path} && mm3d SaisieMasqQT {self.frames[0]} Name="masq_right.tif"')
        saisie_masq_right_cmd.run()

        mkdir_cmd = Command(f'cd {self.path} && mkdir right && cp *.png right && cp -r Homol Pastis right && mv masq_right.* right', save_output=True)
        mkdir_cmd.run()


