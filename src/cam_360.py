from log_aero360 import LogAero360

class Cam360():
    """Classe pour le traitement de caméra 360
    """

    def __init__(self, name:str, f:float=None, f_35:float=None) -> None:
        """Constructeur de la classe Cam360

        :param name: nom de la caméra
        :type name: str
        :param f: focale de la caméra, defaults to None
        :type f: float, optional
        :param f_35: focale équivalent 35mm, defaults to None
        :type f_35: float, optional
        """
        self.name = name
        self.f = f
        self.f_35 = f_35

        print(LogAero360(f'La caméra {self.name} a été initialisée.', event='okcyan'))