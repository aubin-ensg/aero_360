from frame_stack_360 import FrameStack360
import numpy as np
from command import Command
from equi2 import hours_2_sec, datetime_2_sec
import pandas as pd
from cam_360 import Cam360
from log_aero360 import LogAero360


class DumbStack360(FrameStack360):
    """Classe qui contient un paquet de frames extraites d'une PVA360 et générées par la méthode dumb_frames()

    :param FrameStack360: classe mère
    :type FrameStack360: FrameStack360
    """

    def __init__(self,path:str, gps_data:str=None, fps=1, offset='00:00:00', camera:Cam360=Cam360('gpmax', f=8.9, f_35=49.)) -> None:
        """Constructeur de la classe DumbStack360

        :param path: chemin vers le dossier dumb_stack du répertoire de travail
        :type path: str
        :param gps_data: chemin vers le fichier de données gps, defaults to None
        :type gps_data: str, optional
        :param fps: fréquence d'échantillonage selon laquelle les images ont été extraites, defaults to 1
        :type fps: int, optional
        :param offset: décalage du début de la vidéo traitée par rapport au début de la vidéo d'origine, defaults to '00:00:00'
        :type offset: str, optional
        :param camera: caméra avec laquelle ont été captée les images, defaults to Cam360('gpmax', f=8.9, f_35=49.)
        :type camera: Cam360, optional
        """
        super().__init__(path)
        self.gps_data = gps_data
        self.fps = fps
        self.offset = hours_2_sec(offset)
        self.camera = camera

        print(LogAero360(f'Le DumbStack {self.path} a été initialisé.', event='okcyan'))


    def set_gps_position(self) -> None:
        """Associe à chaque image une position gps et crée un fichier 'gps_images_positions.txt' qui reprend cette association au format MicMac
        """

        self.gps_positions = self.read_gps_data(self.gps_data)

        if self.gps_positions is None:
            return

        with open(self.path+'/gps_images_positions.txt', 'w', encoding='utf8') as fd:
            lines = ['#F=N X Y Z\n', '#image longitude latitude altitude\n',]
            first_time = self.gps_positions[0,0]
            for img in self.frames:
                n_image = int(img.split('/')[-1].split('_')[-1][:-4])
                img_gps_time =  first_time+self.offset+n_image*1/self.fps
                closest_position_index = np.argmin(abs(self.gps_positions[:,0]-img_gps_time*np.ones(self.gps_positions[:,0].shape)))
                exif_tool_cmd = Command(f'exiftool \
                                        -GPSLongitude={self.gps_positions[closest_position_index,1]} \
                                        -GPSLatitude={self.gps_positions[closest_position_index,2]} \
                                        -GPSAltitude={self.gps_positions[closest_position_index,3]} {img} -m', save_output=True)
                exif_tool_cmd.run()
                lines.append(f"{img.split('/')[-1]} \
                             {self.gps_positions[closest_position_index,1]} \
                             {self.gps_positions[closest_position_index,2]} \
                             {self.gps_positions[closest_position_index,3]}\n")

            fd.writelines(lines)
        clean_exif_tool_cmd = Command(f'cd {self.path} && rm *.png_original')
        clean_exif_tool_cmd.run()

    def read_gps_data(self, gps_data:str=None) -> np.ndarray:
        """Lit le fichier de données gps

        :param gps_data: chemin vers le fichier de données, defaults to None
        :type gps_data: str, optional
        :return: tableau qui contient les positions et les temps associés
        :rtype: np.ndarray
        """
        if gps_data is None:
            return None
        
        if self.camera.name == 'gpmax':
            df = pd.read_csv(gps_data, delimiter=';')
            df['gps_time'] = df['Time'].apply(datetime_2_sec)
            gps_pos = np.array((df['gps_time'],df['Lng'], df['Lat'],df['Alt'])).T

        return gps_pos
    
    def ori_convert(self):
        pass
        #mm3d OriConvert OriTxtInFile positions_orientations.txt PosOri