from command import Command
from log_aero360 import LogAero360
from cam_360 import Cam360
from pathlib import Path
from shell import shell_colors
import numpy as np
from PIL import Image
import matplotlib.image
import pandas as pd
from equi2 import hours_2_sec, datetime_2_sec
from frame_stack_360 import FrameStack360
from dumb_stack_360 import DumbStack360

class PVA360():
    """Classe pour le traitement d'une prise de vue 360
    """

    def __init__(self, video:str, camera:Cam360, gps_data:str=None, offset='00:00:00') -> None:
        """Constructeur de la classe PVA360

        :param video: chemin vers la vidéo de prise de vue (format mp4)
        :type video: str
        :param camera: caméra utilisée lors de la prise de vue
        :type camera: Cam360
        :param gps_data: chemin vers le fichiers de données gps (format GoPro Max), defaults to None
        :type gps_data: str, optional
        :param offset: décalage du début de la vidéo traitée par rapport au début de la vidéo d'origine (si vidéo non originale), defaults to '00:00:00'
        :type offset: str, optional
        """
    
        self.path = Path(video)
        self.name = str(self.path.resolve()).split('/')[-1][:-4]
        self.extension = str(self.path.resolve()).split('/')[-1][-4:]
        self.mmdir = self.path.parent.absolute()
        self.camera = camera
        self.gps_data = gps_data
        self.offset = offset

        print(LogAero360(f'La vidéo 360 {self.name} a été initialisée.', event='okcyan'))

    def cut(self, t_start:str, duration:str):
        """Extrait une séquence de la vidéo d'origine, passée en paramètre du constructeur

        :param t_start: début de la vidéo (format hh:mm:ss)
        :type t_start: str
        :param duration: durée de l'extrait (format hh:mm:ss)
        :type duration: str
        :return: prise de vue 360 extraite de la vidéo d'origine, passée en paramètre du constructeur
        :rtype: PVA360
        """
        self.offset = t_start

        cut_cmd = Command(f'cd {self.mmdir} && ffmpeg -y -hide_banner -loglevel error -i {self.path.resolve()} -ss {self.offset} -t {duration} -c copy {str(self.path.resolve())[:-4]}_cut.mp4', 
                          save_output=True)
        cut_cmd.run()
        
        if cut_cmd.sucess:
            self.path_cut = f'{str(self.path.resolve())[:-4]}_cut.mp4'
            if self.gps_data :
                return PVA360(self.path_cut, self.camera, gps_data=self.gps_data, offset=self.offset)
            else:
                return PVA360(self.path_cut, self.camera, offset=self.offset)
        
    def smart_frames(self, rate:int=None, overlap:int=None, overwrite:bool=True) -> FrameStack360:
        """Découpe la vidéo passée en paramètre du constructeurs en un certain nombre de frames en utilisant la commande DIV de MicMac

        :param rate: ratio d'images extraites à la seconde, defaults to None
        :type rate: int, optional
        :param overlap: recouvrement cible entre images successives, defaults to None
        :type overlap: int, optional
        :param overwrite: détermine la supression de dossiers déjà créés, defaults to True
        :type overwrite: bool, optional
        :return: objet qui contient l'ensemble des frames générées enregistrées dans le dossier smart_frames du répertoire de travail
        :rtype: FrameStack360
        """
        
        if Path(f'{self.mmdir}/smart_frames').exists() and not overwrite:
            log1 = LogAero360('Le dossier ./smart_frames existe déjà. Passer le paramètre <overwrite=True> pour en créer un nouveau.')
            print(log1)
            return
        
        rm_dir_cmd = Command(f'cd {self.mmdir} && rm -rf smart_frames')
        rm_dir_cmd.run()

        mkdir_cmd = Command(f'cd {self.mmdir} && mkdir smart_frames -p')
        mkdir_cmd.run()

        copy_cmd = Command(f'cd {self.mmdir}/smart_frames && cp {self.path.resolve()} . -n')
        copy_cmd.run()

        div_cmd = Command(f'cd {self.mmdir}/smart_frames && mm3d DIV {self.mmdir}/smart_frames/{self.name}{self.extension}', save_output=True)

        if self.camera.f_35:
            div_cmd.cmd += f' {self.camera.f_35}'
        else:
            div_cmd.cmd += ' -1'

        if self.camera.f:
            div_cmd.cmd += f' Foc={self.camera.f}'
        
        if rate:
            div_cmd.cmd += f' Rate={rate}'
        if overlap:
            div_cmd.cmd += f' OverLap={overlap}'
        
        div_cmd.run()

        clean_micmac_cmd = Command(f'cd {self.mmdir}/smart_frames && mkdir POUB && mv *_Nl.png Homol Pastis Tmp-MM-Dir *txt POUB && rm {self.name}{self.extension}')
        clean_micmac_cmd.run()

        return FrameStack360(f'{self.mmdir}/smart_frames')


    def dumb_frames(self, fps:int=3, overwrite:bool=True) -> DumbStack360:
        """Découpe la vidéo passée en paramètre du constructeurs en un certain nombre de frames défini par le paramètre <fps>

        :param fps: frames par seconde, fréquence d'échantillonnage de la vidéo, defaults to 3
        :type fps: int, optional
        :param overwrite: détermine la supression de dossiers déjà créés, defaults to True
        :type overwrite: bool, optional
        :return: objet qui contient l'ensemble des frames générées enregistrées dans le dossier dumb_frames du répertoire de travail
        :rtype: DumbStack360
        """

        if Path(f'{self.mmdir}/dumb_frames').exists() and not overwrite:
            log1 = LogAero360('Le dossier ./dumb_frames existe déjà. Passer le paramètre <overwrite=True> pour en créer un nouveau.')
            print(log1)
            return

        print(LogAero360(f'Extraction des frames...', event='bold'))

        rm_dir_cmd = Command(f'cd {self.mmdir} && rm -rf dumb_frames')
        rm_dir_cmd.run()

        mkdir_cmd = Command(f'cd {self.mmdir} && mkdir dumb_frames -p', save_output=True)
        mkdir_cmd.run()
        
        ffmpeg_cmd = Command(f'cd {self.mmdir}/dumb_frames && ffmpeg -i {self.path.resolve()} -vf fps={fps} {self.name}_%d.png', save_output=True)
        ffmpeg_cmd.run()

        print(LogAero360(f'Les frames ont été extraites.', event='bold'))


        return DumbStack360(f'{self.mmdir}/dumb_frames', self.gps_data, fps=fps, offset=self.offset)
    