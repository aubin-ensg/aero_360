import numpy as np
from PIL import Image
import matplotlib.image

def concat(left_img:str, right_img:str, concat_img:str):
    """Concatène  deux images de même dimensions horizontalement

    :param left_img: chemin de l'image de gauche
    :type left_img: str
    :param right_img: chemin de l'image de droite
    :type right_img: str
    :param concat_img: chemin de l'image résultante
    :type concat_img: str
    :return: tableau numpy de l'image concaténée
    :rtype: np.ndarray
    """
    left = Image.open(left_img)
    right = Image.open(right_img)
    concat = np.hstack((np.asarray(left), np.asarray(right)))
    matplotlib.image.imsave(concat_img, concat)
    
    return concat

def equi_2_split_equi(img:str) -> bool:
    """Découpe une image projetée en EAC vers deux images correspondant à chacune des deux images acquises par chacune des lentilles d'une caméra 360

    :param img: chemin vers l'image à découper
    :type img: str
    :return: traduit le succès de l'opération
    :rtype: bool
    """

    equi = Image.open(img)
    w = equi.width
    array = np.asarray(equi)
    
    crop1=array[:,:int(w/4),:]
    crop2=array[:,int(w/4):int(w/2),:]
    crop3=array[:,int(w/2):int(3*w/4),:]
    crop4=array[:,int(3*w/4):w,:]

    crop_left = np.hstack((crop4, crop1))
    crop_right = np.hstack((crop2,crop3))

    matplotlib.image.imsave(f'{img[:-4]}_left.png', crop_left)
    matplotlib.image.imsave(f'{img[:-4]}_right.png', crop_right)

    return True

def equi_2_fish_eye(img:str):
    """Déprojete une image projetée en EAC vers une image fish-eye

    :param img: chemin de l'image projetée
    :type img: str
    :return: traduit le succès de l'opération
    :rtype: bool
    """

    ac = (180*np.pi)/180
    equi = Image.open(img)
    array = np.asarray(equi)
    
    wf = equi.width
    hf = equi.height
    
    x=np.concatenate([np.arange(wf).reshape(1,wf) for i in np.arange(wf)])
    y=np.concatenate([np.arange(hf).reshape(hf,1) for i in np.arange(hf)],axis=1)
    
    x = 2*(x/(wf-1)-0.5)
    y = 2*(y/(hf-1)-0.5)
    
    r = np.sqrt(x**2 + y**2)# 0 si distance supérieure à 1 (au delà du cercle)

    outside_fish_eye = np.where(r>1, 0, 1)
    
    r *= outside_fish_eye
    
    phi = r*ac/2
    theta = np.arctan2(y, x)
    px = np.sin(phi) * np.cos(theta)
    py = np.cos(phi)
    pz = np.sin(phi) * np.sin(theta)
    
    long = np.arctan2(py, px)
    lat = np.arctan2(pz, np.sqrt(px**2+py**2))
    
    x_eq = (long / np.pi) * wf
    y_eq = (2 * lat / np.pi + 1) / 2 * hf
                    
    x_eq *= outside_fish_eye
    y_eq *= outside_fish_eye
    fish_eye_array =  array[y_eq.astype(int), x_eq.astype(int)]
    fish_eye_array = np.fliplr(fish_eye_array)[:,:,:3]
    matplotlib.image.imsave(f'{img[:-4]}_fish_eye.png', fish_eye_array)

    return True

def datetime_2_sec(date:str) -> float :
    """Convertit une date du format "yyyy-mm-jj hh:mm:ss" en secondes (seule l'heure est prise en compte)

    :param date: date au format "yyyy-mm-jj hh:mm:ss" à convertir 
    :type date: str
    :return: temps convertit en secondes
    :rtype: float
    """
    time = date.split(' ')[1]
    h = time.split(':')[0]
    m = time.split(':')[1]
    s = time.split(':')[2]

    return float(h)*3600+float(m)*60+float(s)

def hours_2_sec(time:str) -> float :
    """Convertit un temps du format "hh:mm:ss" en secondes

    :param time: Temps à convertir
    :type time: str
    :return: temps convertit en secondes
    :rtype: float
    """
    h = time.split(':')[0]
    m = time.split(':')[1]
    s = time.split(':')[2]

    return float(h)*3600+float(m)*60+float(s)


