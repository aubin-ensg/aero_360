from shell import shell_colors

class LogAero360():
    """Classe pour l'affichage de logs colorés dans la console
    """

    def __init__(self, msg:str, event:str='warning') -> None:
        """Constructeur de la classe

        :param msg: message à afficher
        :type msg: str
        :param event: évenement à l'origine du message, defaults to 'warning'
        :type event: str, optional
        """
        self.msg = msg
        self.event = event

    def __str__(self) -> str:
        """Surcharge de la méthode __str__() pour pouvoir utiliser la fonction built-in print()

        :return: message coloré
        :rtype: str
        """
        return shell_colors[self.event] + 'AERO360 : ' + shell_colors['endc'] + self.msg