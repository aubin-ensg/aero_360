import sys
sys.path.insert(1, '../src/')
from command import Command
from pva_360 import PVA360
from cam_360 import Cam360
from frame_stack_360 import FrameStack360
from dumb_stack_360 import DumbStack360

if __name__ == '__main__':
    #--1--Création d'un objet Cam360-----#
    #gpmax = Cam360('gpmax', f=8.9, f_35=49.)

    #--2--Création d'un objet PVA360-----#
    #gpmax_drone_cut = PVA360('./GS025853/GS025853_02_10.mp4', gpmax, gps_data='./GS025853/00000073_GPS_2.csv')

    #la vidéo GS025853_02_10.mp4 est un extrait de GS025853.mp4 trop lourde pour être téléchargée#
    #voici les deux étapes qui permettent de créér GS025853_02_10.mp4 :#
    #1#--gpmax_drone = PVA360('./GS025853/GS025853.mp4', gpmax, gps_data='./GS025853/00000073_GPS_2.csv')--##
    #2#--gpmax_done_cut = gpmax_drone.cut('00:02:00', '00:00:10')--##

    #--3--Extraction des frames (méthode ffmpeg)-----#
    #dumb_stack = gpmax_drone_cut.dumb_frames()

    #--4--Déprojection des frames-----#
    #dumb_stack.deproj_frames()

    #--5--Concaténation des images des deux lentilles-----#
    #à partir de cette étape, pour éviter de générer à nouveau les images commenter toutes les lignes au-dessus#
    #et décommenter les deux suivantes#

    dumb_stack = DumbStack360('/home/aero360/abettiol/aero_360/workshop/GS025853/dumb_frames',
                              gps_data='/home/aero360/abettiol/aero_360/workshop/GS025853/00000073_GPS_2.csv',
                              fps=3, offset='00:02:00',
                              camera=Cam360('gpmax', f=8.9, f_35=49.))
    
    dumb_stack.concat_left_right()

    #--6--Configuration des métadonnées EXIF-----
    dumb_stack.set_exif(Cam360('gpmax', f=8.9, f_35=49.))
    
    #--7--Association des positions GPS-----
    dumb_stack.set_gps_position()

    #--8--Détection des points d'intérêts-----
    dumb_stack.tapioca()

    #--9--Saisie des masques gauche/droite-----
    dumb_stack.saisie_masq()

    